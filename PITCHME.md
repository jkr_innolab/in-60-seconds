# Innolab

@snap[west]
@fab[docker]
@snapend

@snap[east]
@fab[python]
@snapend

---

## What will this do?

##### The Cauchy-Schwarz Inequality

`\[
\left( \sum_{k=1}^n a_k b_k \right)^{\!\!2} \leq
 \left( \sum_{k=1}^n a_k^2 \right) \left( \sum_{k=1}^n b_k^2 \right)
\]`

+++

### and what about this?

---

Regular content

@snap[west]
# Greetings
@snapend

Note:

- Here are
- some notes
- to self
